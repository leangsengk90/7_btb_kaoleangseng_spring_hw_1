package com.kshrd.rest.restController;

import com.kshrd.models.BookDto;
import com.kshrd.models.CategoryDto;
import com.kshrd.repositories.dto.CategoryRepositories;
import com.kshrd.rest.request.BookRequestModel;
import com.kshrd.rest.request.CategoryRequestModel;
import com.kshrd.rest.response.BaseApiResponse;
import com.kshrd.services.Impl.CategoryServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CategoryRestController {

    private CategoryServiceImpl categoryService;

    public CategoryRestController() {
    }

    @Autowired
    public CategoryRestController(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }


    @PostMapping("/category")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert(
            @RequestBody CategoryDto category
    ){
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        categoryService.insert(category);
        CategoryDto categoryDto = categoryService.selectLastID();
        CategoryRequestModel request = mapper.map(categoryDto,CategoryRequestModel.class);

        response.setMessage("Insert category successfully!");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> deleteById(@PathVariable int id){
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        CategoryDto categoryDto = categoryService.findOne(id);
        //System.out.println("FindONE: "+bookDto.toString());

        CategoryRequestModel request = mapper.map(categoryDto,CategoryRequestModel.class);
        response.setMessage("Delete category by ID successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        categoryService.deleteById(id);
        return  ResponseEntity.ok(response);
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> findOne(@PathVariable int id){
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        CategoryDto categoryDto = categoryService.findOne(id);
        CategoryRequestModel request = mapper.map(categoryDto, CategoryRequestModel.class);

        response.setMessage("Show category by ID successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @GetMapping("/category")
    public ResponseEntity<BaseApiResponse<List<CategoryRequestModel>>> findAll(){
        BaseApiResponse<List<CategoryRequestModel>> response = new BaseApiResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        List<CategoryDto> categoryDto1 = categoryService.findAll();
        List<CategoryRequestModel> request = new ArrayList<>();
        for(CategoryDto categoryDto2: categoryDto1){
            request.add(modelMapper.map(categoryDto2, CategoryRequestModel.class));
        }

        response.setMessage("Show all categories successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> updateById(
            @PathVariable int id, @RequestBody CategoryDto category
    ){
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        categoryService.updateById(id, category);
        CategoryDto categoryDto = categoryService.findOne(id);
        CategoryRequestModel request = mapper.map(categoryDto,CategoryRequestModel.class);

        response.setMessage("Updated category by ID successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

}
