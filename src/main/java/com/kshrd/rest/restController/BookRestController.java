package com.kshrd.rest.restController;


import com.kshrd.models.BookDto;
import com.kshrd.rest.request.BookRequestModel;
import com.kshrd.rest.response.BaseApiResponse;
import com.kshrd.services.Impl.BookServiceImpl;
import com.kshrd.services.Impl.FileUploadService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class BookRestController {

    private BookServiceImpl booksService;

    @Autowired
    public BookRestController(BookServiceImpl booksService) {
        this.booksService = booksService;
    }

    @PostMapping("/book")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(
            @RequestBody BookDto book
    ){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();

        booksService.insert(book);
        BookDto bookDto = booksService.selectLastID();
        BookRequestModel request = mapper.map(bookDto,BookRequestModel.class);

        response.setMessage("Insert book successfully!");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/book/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> deleteById(@PathVariable int id){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        BookDto bookDto = booksService.findOne(id);
        //System.out.println("FindONE: "+bookDto.toString());

        BookRequestModel request = mapper.map(bookDto,BookRequestModel.class);
        response.setMessage("Delete book by ID successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        booksService.deleteById(id);
        return  ResponseEntity.ok(response);
    }

    @GetMapping("/book/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> findOne(@PathVariable int id){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        BookDto bookDto = booksService.findOne(id);
        BookRequestModel request = mapper.map(bookDto, BookRequestModel.class);

        response.setMessage("Show book by ID successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @GetMapping("/book")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> findAll(){
        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        List<BookDto> bookDto1 = booksService.findAll();
        List<BookRequestModel> request = new ArrayList<>();
        for(BookDto bookDto2: bookDto1){
            request.add(modelMapper.map(bookDto2,BookRequestModel.class));
        }

        response.setMessage("Show all books successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @PutMapping("/book/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> updateById(
            @PathVariable int id, @RequestBody BookDto book
    ){
        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        booksService.updateById(id, book);
        BookDto bookDto = booksService.findOne(id);
        BookRequestModel request = mapper.map(bookDto,BookRequestModel.class);

        response.setMessage("Updated book by ID successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

    @GetMapping("/search")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> searchTitles(
            @RequestParam  Optional<String> book, @RequestParam  Optional<String> category
    ){
        System.out.println("BOOK: "+book+", CATEGORY: "+category);
        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        List<BookDto> bookDto1 = booksService.searchTitles(book, category);
        List<BookRequestModel> request = new ArrayList<>();
        for(BookDto bookDto2: bookDto1){
            request.add(modelMapper.map(bookDto2,BookRequestModel.class));
        }

        response.setMessage("Search all books successfully");
        response.setData(request);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return  ResponseEntity.ok(response);
    }

}
