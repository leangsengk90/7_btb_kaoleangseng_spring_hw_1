package com.kshrd.rest.restController;

import com.kshrd.services.Impl.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UploadRestController {

    private FileUploadService fileUploadService;

    @Autowired
    public void setFileUploadService(FileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    @PostMapping("/upload")
    public void uploadFile(@RequestParam("file") MultipartFile file){
        fileUploadService.uploadFile(file);
    }

    @GetMapping("/upload/{image}")
    public String getFile(String image){
        image = "3b2d68_0ea2a45a5afc429ebc28aca9d02b5951_mv2.jpg";
        return "http://localhost:8080/api/v1/upload/"+image;
    }
}
