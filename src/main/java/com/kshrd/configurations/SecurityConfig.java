package com.kshrd.configurations;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                //.password("{noop}123")
                .password(pass().encode("123"))
                .roles("ADMIN","USER");
        auth.inMemoryAuthentication()
                .withUser("user")
                .password(pass().encode("123"))
                .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/v1/**").hasRole("ADMIN")
                .antMatchers("/api/v1/book").hasRole("USER")
                .antMatchers("/api/v1/search").hasRole("USER")
                .and()
                .httpBasic();
    }

    @Bean
    public PasswordEncoder pass(){
        return new BCryptPasswordEncoder();
    }

}
