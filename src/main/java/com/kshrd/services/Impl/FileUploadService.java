package com.kshrd.services.Impl;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Service
public class FileUploadService {
    public void uploadFile(MultipartFile file){
        try {
            file.transferTo(new File("/Users/leangsengkao/Documents/KSHRDDocuments/Spring/book-management/upload/"+file.getOriginalFilename()));

        }catch (Exception e){
            System.out.println(e);
        }
    }
}
