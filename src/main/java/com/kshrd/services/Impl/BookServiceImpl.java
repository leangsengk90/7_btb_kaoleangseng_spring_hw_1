package com.kshrd.services.Impl;

import com.kshrd.models.BookDto;
import com.kshrd.repositories.dto.BookRepositories;
import com.kshrd.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    private BookRepositories bookRepositories;

    @Autowired
    public BookServiceImpl(BookRepositories bookRepositories) {
        this.bookRepositories = bookRepositories;
    }

    @Override
    public BookDto insert(BookDto bookDto) {
        boolean isInserted = bookRepositories.insert(bookDto);
        if (isInserted) return bookDto;
        else return null;
    }

    @Override
    public BookDto selectLastID() {
        return bookRepositories.selectLastID();
    }

    @Override
    public boolean deleteById(int id) {
        boolean isDeleted = bookRepositories.deleteById(id);
        return isDeleted;
    }

    @Override
    public BookDto findOne(int id) {
        return bookRepositories.findOne(id);
    }

    @Override
    public List<BookDto> findAll() {
        return bookRepositories.findAll();
    }

    @Override
    public boolean updateById(int id, BookDto book) {
        boolean isUpdated = bookRepositories.updateById(id, book);
        return isUpdated;
    }

    @Override
    public List<BookDto> searchTitles(Optional<String> book,  Optional<String> category) {
        return bookRepositories.searchTitles(book, category);
    }


}
