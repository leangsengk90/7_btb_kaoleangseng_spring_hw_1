package com.kshrd.services.Impl;

import com.kshrd.models.CategoryDto;
import com.kshrd.repositories.dto.CategoryRepositories;
import com.kshrd.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepositories categoryRepositories;

    @Autowired
    public CategoryServiceImpl(CategoryRepositories categoryRepositories) {
        this.categoryRepositories = categoryRepositories;
    }

    @Override
    public CategoryDto insert(CategoryDto categoryDto) {
        boolean isInserted = categoryRepositories.insert(categoryDto);
        if(isInserted) return categoryDto;
        else return null;
    }

    @Override
    public boolean deleteById(int id) {
        boolean isDeleted = categoryRepositories.deleteById(id);
        return isDeleted;
    }

    @Override
    public CategoryDto findOne(int id) {
        return categoryRepositories.findOne(id);
    }

    @Override
    public CategoryDto selectLastID() {
        return categoryRepositories.selectLastID();
    }

    @Override
    public List<CategoryDto> findAll() {
        return categoryRepositories.findAll();
    }

    @Override
    public boolean updateById(int id, CategoryDto category) {
        boolean isUpdated = categoryRepositories.updateById(id, category);
        return  isUpdated;
    }
}
