package com.kshrd.services;

import com.kshrd.models.BookDto;

import java.util.List;
import java.util.Optional;

public interface BookService {

    BookDto insert(BookDto bookDto);
    BookDto selectLastID();
    boolean deleteById(int id);
    BookDto findOne(int id);
    List<BookDto> findAll();
    boolean updateById(int id, BookDto book);
    List<BookDto> searchTitles(Optional<String> book,  Optional<String> category);
}
