package com.kshrd.services;

import com.kshrd.models.BookDto;
import com.kshrd.models.CategoryDto;

import java.util.List;

public interface CategoryService {
    CategoryDto insert(CategoryDto categoryDto);
    boolean deleteById(int id);
    CategoryDto findOne(int id);
    CategoryDto selectLastID();
    List<CategoryDto> findAll();
    boolean updateById(int id, CategoryDto category);
}
