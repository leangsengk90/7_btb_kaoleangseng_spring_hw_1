package com.kshrd.repositories;

import com.kshrd.models.BookDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

public class BookProvider {

    public String deleteById(int id){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String findOne(int id){
        return new SQL(){{
            SELECT("b.id, b.title, b.author, b.description, b.thumbnail, c.id as cid, c.title as ctitle");
            FROM("tb_books b");
            INNER_JOIN("tb_categories c ON b.category_id = c.id");
            WHERE("b.id = #{id}");
        }}.toString();
    }

    public String updateById(int id, @Param("book") BookDto book){
        return new SQL(){{
            UPDATE("tb_books");
            SET("title  = #{book.title}");
            SET("author = #{book.author}");
            SET("description = #{book.description}");
            SET("thumbnail = #{book.thumbnail}");
            SET("category_id = #{book.category_id}");
            WHERE("id = "+id);
        }}.toString();
    }

    public String searchTitles(@RequestParam("book") Optional<String> book, @RequestParam("category")  Optional<String> category){
        return new SQL(){{
            SELECT("b.id, b.title, b.author, b.description, b.thumbnail, c.id as cid, c.title as ctitle");
            FROM("tb_books b");
            INNER_JOIN("tb_categories c ON b.category_id = c.id");
            WHERE("LOWER(b.title) LIKE '"+book.orElse("*").toLowerCase()+"%' OR LOWER(c.title) LIKE '"+category.orElse("*").toLowerCase()+"%'");
        }}.toString();
    }
}
