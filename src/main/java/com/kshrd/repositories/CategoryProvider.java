package com.kshrd.repositories;

import com.kshrd.models.BookDto;
import com.kshrd.models.CategoryDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String deleteById(int id){
        return new SQL(){{
            DELETE_FROM("tb_categories");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String findOne(int id){
        return new SQL(){{
            SELECT("id, title");
            FROM("tb_categories");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String updateById(int id, @Param("category") CategoryDto category){
        return new SQL(){{
            UPDATE("tb_categories");
            SET("title = #{category.title}");
            WHERE("id = "+id);
        }}.toString();
    }
}
