package com.kshrd.repositories.dto;

import com.kshrd.models.CategoryDto;
import com.kshrd.models.UploadDto;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadRepositories {

    @Insert("INSERT INTO tb_upload(title) " +
                   "VALUES(#{title});")
    boolean insert(UploadDto uploadDto);
}
