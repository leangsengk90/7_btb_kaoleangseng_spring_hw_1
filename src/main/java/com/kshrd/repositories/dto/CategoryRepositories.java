package com.kshrd.repositories.dto;

import com.kshrd.models.BookDto;
import com.kshrd.models.CategoryDto;
import com.kshrd.repositories.BookProvider;
import com.kshrd.repositories.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepositories {

    @Insert("INSERT INTO tb_categories(title) " +
            "VALUES(#{title});")
    boolean insert(CategoryDto categoryDto);

    @DeleteProvider(type = CategoryProvider.class, method="deleteById")
    boolean deleteById(int id);

    @SelectProvider(type = CategoryProvider.class, method = "findOne")
    CategoryDto findOne(int id);

    @Select("SELECT id, title FROM tb_categories")
    List<CategoryDto> findAll();

    @Select("SELECT id, title FROM tb_categories ORDER BY id DESC LIMIT 1")
    CategoryDto selectLastID();

    @UpdateProvider(type = CategoryProvider.class, method = "updateById")
    boolean updateById(int id, @Param(value = "category") CategoryDto category);

}
