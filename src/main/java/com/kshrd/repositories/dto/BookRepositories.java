package com.kshrd.repositories.dto;

import com.kshrd.models.BookDto;
import com.kshrd.repositories.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;
import sun.jvm.hotspot.debugger.Page;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepositories {

    @Insert("INSERT INTO tb_books(title,author,description,thumbnail,category_id) " +
            "VALUES(#{title},#{author},#{description},#{thumbnail},#{category_id});")
    boolean insert(BookDto bookDto);

    @Select("SELECT b.id, b.title, b.author, b.description, b.thumbnail, c.id as cid, c.title as ctitle " +
            "FROM tb_books b INNER JOIN tb_categories c " +
            "ON b.category_id = c.id " +
            "ORDER BY b.id DESC LIMIT 1;")
    @Results(value = {
            @Result(property = "category.id", column = "cid"),
            @Result(property = "category.title", column = "ctitle"),
    })
    BookDto selectLastID();

    @DeleteProvider(type = BookProvider.class, method="deleteById")
    boolean deleteById(int id);

    @SelectProvider(type = BookProvider.class, method = "findOne")
    @Results(value = {
            @Result(property = "category.id", column = "cid"),
            @Result(property = "category.title", column = "ctitle"),
    })
    BookDto findOne(int id);

    @Select("SELECT b.id, b.title, b.author, b.description, b.thumbnail, c.id as cid, c.title as ctitle " +
            "FROM tb_books b INNER JOIN tb_categories c " +
            "ON b.category_id = c.id;")
    @Results(value = {
            @Result(property = "category.id", column = "cid"),
            @Result(property = "category.title", column = "ctitle"),
    })
    List<BookDto> findAll();

    @UpdateProvider(type = BookProvider.class, method = "updateById")
    boolean updateById(int id, @Param(value = "book") BookDto book);

    @SelectProvider(type=BookProvider.class, method = "searchTitles")
    @Results(value = {
            @Result(property = "category.id", column = "cid"),
            @Result(property = "category.title", column = "ctitle"),
    })
    List<BookDto> searchTitles(@RequestParam(value = "book") Optional<String> book, @RequestParam(value = "category")  Optional<String> category);
}
